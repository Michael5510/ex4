<?php
    include "books.php";
    include "query.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>
    </head>
    <body>
        <p>
        <?php
            $db = new DB('localhost', 'intro', 'root', '');
            $dbc= $db->connect();
            $query = new Query($dbc); #יצירת אובייקה
           # $q = "SELECT * FROM books INNER JOIN users ON books.users_fk = users.id";
            $q = "SELECT * FROM books, users WHERE books.users_fk = users.id";
            $result = $query ->query($q); #הרצת השאילתה
            if($result->num_rows > 0){
            echo '<table>'; 
            echo '<tr><th>Books name</th><th>Users</th></tr>';
            while($row = $result->fetch_assoc()){
                echo '<tr>'; 
                echo '<td>'.$row['title_field'].'</td><td>'.$row['name'].'</td>';
                echo '</tr>';
            }         
            echo '</table>';
    
            } else{ 
                 echo "Sorry no results";
            }  

        ?>